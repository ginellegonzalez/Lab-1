package orderedStructures;

public class Arithmetic extends Progression {
	private double commonDifference; 
	
	public Arithmetic(double firstValue, double commonDifference) { 
		super(firstValue); 
		this.commonDifference = commonDifference; 
	}
	
	@Override
	public double nextValue() {
		//IllegalStateException, problem 6
		if (usedFirstValue = false) 
			throw new IllegalStateException("nextValue: Needs to use firstValue method first");
		current = current + commonDifference; 
		return current;
	}
	//problem 3
		@Override
		public String toString(){
			String output = "Arith("+ firstValue() + "," + this.commonDifference + ")";
			return output;
		}
		
		//problem 5 
		@Override
		public double getTerm(int n) throws IndexOutOfBoundsException { 
			if (n <= 0) 
				throw new IndexOutOfBoundsException("printAllTerms: Invalid argument value = " + n); 
			
			return firstValue() + commonDifference*(n-1);
		}


}
