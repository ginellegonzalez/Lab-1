package orderedStructures;

public class Geometric extends Progression {

	private double commonFactor; 
	
	public Geometric(double firstValue, double commonFactor) { 
		super(firstValue); 
		this.commonFactor = commonFactor; 
	}
	
	@Override
	public double nextValue() {
		//IllegalStateException, problem 6
		if (usedFirstValue = false) 
			throw new IllegalStateException("nextValue: Needs to use firstValue method first");
		current = current * commonFactor; 
		return current;
	}
	//problem 3
		@Override
		public String toString(){
			String output = "Geom("+ firstValue() + "," + this.commonFactor + ")";
			return output;
		}
		
		//problem 5
		@Override
		public double getTerm(int n) throws IndexOutOfBoundsException { 
			if (n <= 0) 
				throw new IndexOutOfBoundsException("printAllTerms: Invalid argument value = " + n); 
			
			return firstValue() * Math.pow(commonFactor, n - 1);
		}
}
